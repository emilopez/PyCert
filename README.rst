1- Armar SVG con etiquetas, por ejemplo {{name}}

2- Convertir tu archivo CSV en YAML usando csv2yaml.py (fijate qué campos y columnas te interesan o si necesitás algún procesamiento)

3- Generar el PDF de cada certificado usando certg.py de Facundo Batista

4- Armar un único PDF concatenando todos los PDFs individuales usando joinpdf.py
