# -*- coding: utf-8 -*-
#!/usr/bin/env python3

import yaml
import csv

fh = open('base.yaml', 'rt')
f2 = open('PyDay2015.yaml', 'w')

config = yaml.load(fh)

#Columna del apellido y nombre y respuesta si desea certificado
col_apenom = 1
col_cert = 6
col_dni = 8

listadni = []
with open('Inscripcion-PyDay-Litoral-2015.csv', 'rb') as csvfile:
    lista_inscriptos = csv.reader(csvfile, delimiter=',', quotechar='|')
    for campo in lista_inscriptos:
        quiere_cert = campo[col_cert]
        nombre = campo[col_apenom]
        dni = campo[col_dni]
        if quiere_cert == 'Si':
            if dni == '':
                dni = '........................'
            config['replace_info'].append({'stopit': '.', 'name': nombre, 'dni': dni})

f2.write(yaml.dump(config, encoding=None))
